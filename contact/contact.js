$("form").submit(event => {
    event.preventDefault();

    $.ajax({
        url: "register.php",
        type: "POST",
        dataType: "json",
        data: {
            lastname: $("#lastname").val(),
            phone: $("#telephone").val(),
            email: $("#email").val(),
            content: $("#message").val()
        },
        success: (res) => {
            //? Si la réponse est un succès alors
            if (res.success) window.location.replace("index.html"); //? Je redirige vers la page de connexion
            else alert(res.error); //! J'affiche une boite de dialogue avec l'erreur //! J'affiche une boite de dialogue avec l'erreur
        }
    });
});

